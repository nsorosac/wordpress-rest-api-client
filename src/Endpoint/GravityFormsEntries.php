<?php

namespace Vnn\WpApiClient\Endpoint;

class GravityFormsEntries extends AbstractWpEndpoint
{
    /**
     * {@inheritdoc}
     */
    protected function getEndpoint()
    {
        return '/wp-json/gf/v2/entries';
    }
}
